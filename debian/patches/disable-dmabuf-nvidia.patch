From: Carlos Garcia Campos <cgarcia@igalia.com>
Subject: Disable DMABuf renderer for NVIDIA proprietary drivers
Bug: https://bugs.webkit.org/show_bug.cgi?id=262607
Bug-Debian: https://bugs.debian.org/1039720
Origin: https://github.com/WebKit/WebKit/pull/18614
Index: webkitgtk/Source/WebKit/UIProcess/gtk/AcceleratedBackingStoreDMABuf.cpp
===================================================================
--- webkitgtk.orig/Source/WebKit/UIProcess/gtk/AcceleratedBackingStoreDMABuf.cpp
+++ webkitgtk/Source/WebKit/UIProcess/gtk/AcceleratedBackingStoreDMABuf.cpp
@@ -37,6 +37,7 @@
 #include <WebCore/GLContext.h>
 #include <WebCore/IntRect.h>
 #include <WebCore/PlatformDisplay.h>
+#include <WebCore/PlatformDisplaySurfaceless.h>
 #include <WebCore/ShareableBitmap.h>
 #include <WebCore/SharedMemory.h>
 #include <epoxy/egl.h>
@@ -44,6 +45,7 @@
 #include <wtf/glib/WTFGType.h>
 
 #if USE(GBM)
+#include <WebCore/PlatformDisplayGBM.h>
 #include <drm_fourcc.h>
 #include <gbm.h>
 
@@ -58,6 +60,29 @@ static constexpr uint64_t s_dmabufInvali
 
 namespace WebKit {
 
+static bool isNVIDIA()
+{
+    const char* forceDMABuf = getenv("WEBKIT_FORCE_DMABUF_RENDERER");
+    if (forceDMABuf && strcmp(forceDMABuf, "0"))
+        return false;
+
+    std::unique_ptr<WebCore::PlatformDisplay> platformDisplay;
+#if USE(GBM)
+    const char* disableGBM = getenv("WEBKIT_DMABUF_RENDERER_DISABLE_GBM");
+    if (!disableGBM || !strcmp(disableGBM, "0")) {
+        if (auto* device = WebCore::PlatformDisplay::sharedDisplay().gbmDevice())
+            platformDisplay = WebCore::PlatformDisplayGBM::create(device);
+    }
+#endif
+    if (!platformDisplay)
+        platformDisplay = WebCore::PlatformDisplaySurfaceless::create();
+
+    WebCore::GLContext::ScopedGLContext glContext(WebCore::GLContext::createOffscreen(platformDisplay ? *platformDisplay : WebCore::PlatformDisplay::sharedDisplay()));
+    if (strstr(reinterpret_cast<const char*>(glGetString(GL_VENDOR)), "NVIDIA"))
+        return true;
+    return false;
+}
+
 OptionSet<DMABufRendererBufferMode> AcceleratedBackingStoreDMABuf::rendererBufferMode()
 {
     static OptionSet<DMABufRendererBufferMode> mode;
@@ -73,6 +98,9 @@ OptionSet<DMABufRendererBufferMode> Acce
             return;
         }
 
+        if (isNVIDIA())
+            return;
+
         mode.add(DMABufRendererBufferMode::SharedMemory);
 
         const char* forceSHM = getenv("WEBKIT_DMABUF_RENDERER_FORCE_SHM");
